###########################################
# Utilities for OSDG-WikiData Integration #
###########################################

"""
Created 24.12.2020 
Author LukasP
"""

class NodeCreator:
    """
    Creates nodes in the graph from input data
    """

    def __init__( self , driver ) :
        """
        Initializes NodeCreator Object

        PARAMS: 
        driver (obj) - a NEO4J driver
        """
        self.driver = driver 
        self.node_template = {"q_id" : "" , 
                    "label" : "" , 
                    "label_short" : "", 
                    "loc_id" : "" , 
                    "mag_id" : "", 
                    "part_of_flag" : -1 , 
                    "instance_flag" : -1 , 
                    "subclass_flag" : -1 , 
                    "in_osdg" : 0 }

        self.check_query = ("MATCH (a:Node) WHERE a.q_id = $n_id RETURN a.q_id")
        self.insert_query = """CREATE (a:Node { q_id: $n_id , 
                                        label : $label , 
                                        label_short : $label_short , 
                                        loc_id : $loc_id , 
                                        mag_id : $mag_id , 
                                        part_of_flag : $part_of_flag , 
                                        instance_flag : $instance_flag , 
                                        subclass_flag : $subclass_flag , 
                                        in_osdg : $in_osdg }) """  

    def validate_template( self ):
        """
        Validates the input data vis-a-vis the node templete
        """

        self.node_data = { k : v for k,v in self.node_template.items() }
        for key, value in self.initial_data.items() :
            if key in self.node_data.keys() : 
                self.node_data[key] = value
            else:
                 raise ValueError(f"`{key}` is not allowed. Allowed node attributes: {list(self.node_template.keys)}")

        if self.node_data["q_id"] == "" :
            raise ValueError("Item is missing Wiki Data ID")

        else:
            self.node_id = self.node_data["q_id"]
    

    def check_newness( self ):
        """Checks if node is already in graph"""
        with self.driver.session() as session:
            test = [i for i in session.run(self.check_query , n_id = self.node_id )]
        if test == [] :
            self.new_node = True
        else:
            self.new_node = False


    def insert_node( self ):
        """Inserts node intp Neo4J"""
        with self.driver.session() as session:
            session.run( self.insert_query, n_id = self.node_id , 
                                    label = self.node_data["label"] , 
                                    label_short = self.node_data["label_short"] , 
                                    loc_id = self.node_data["loc_id"] , 
                                    mag_id = self.node_data["mag_id"] , 
                                    part_of_flag = self.node_data["part_of_flag"] , 
                                    instance_flag = self.node_data["instance_flag"] ,
                                    subclass_flag = self.node_data["subclass_flag"] ,
                                    in_osdg = self.node_data["in_osdg"] ,
                                    )




    def create_node( self, data_dict):
        """
        Creates a node a node in the graph
        
        PARAMETERS:
        data_dict (dict) - dictionary containing node data
        
        RETURNS
        True if successful 
        """

        self.initial_data = data_dict
        self.validate_template()
        self.check_newness()
        if self.new_node == True :
            pass
        else:
            raise ValueError(f"Node {self.node_id} already exists in graph.")
        self.insert_node()

        return True


class NodeUpdater():
    def __init__( self , driver ) :
        """
        Initializes NodeUpdater Object

        PARAMS: 
        driver (obj) - a NEO4J driver
        """
        self.driver = driver 

        self.query = """MATCH (a:Node) 
                        WHERE a.part_of_flag = -1 OR a.instance_flag =-1 OR a.subclass_flag = -1 
                        RETURN a.q_id"""
    def find_incomplete_nodes( self ):
        with self.driver.session() as session :
            cursor = session.run( self.query )
            self.query_results = [i for i in cursor ]
    
    def update_node( self ):
        """Update status of a single node from 'unchecked' to either 'exists' or 'does not exist'"""
        pass

    def check_for_links():
        """Provides data for validation using SPRQL end-point"""
        pass
    



    





