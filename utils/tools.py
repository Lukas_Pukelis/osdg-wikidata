
def insert_nodes( tx , list_o_dicts ):
    """"
    Inserts nodes into the database

    PARAMETERS:
    tx - Neo4J transaction object
    list_o_dicts - list of dictionaries with node data 

    RETURNS:
    count - number of insertions made 
    """

    node_template = {"q_id" : "" , 
                    "label" : "" , 
                    "label_short" : "", 
                    "loc_id" : "" , 
                    "mag_id" : "", 
                    "part_of_flag" : -1 , 
                    "instance_flag" : -1 , 
                    "subclass_flag" : -1 , 
                    "in_osdg" : 0 }

    #Set nodes to template; check for invalid keys
    data_to_insert = []
    ids = []
    for item in list_o_dicts :
        plh = { k : v for k,v in node_template.items() }
        for key, value in item.items() :
            if key in plh.keys() : 
                plh[key] = value
            else:
                 raise ValueError(f"`{key}` is not allowed. Allowed node attributes: {list(node_template.keys)}")

        if plh["q_id"] == "" :
            raise ValueError("One of more items is missing Wiki Data IDs")

        else:
            data_to_insert.append( plh )
            ids.append(plh["_id"])

    #Check for newness
    check_query = ("MATCH (a:Node) WHERE a.q_id = $n_id RETURN a.q_id")
    insert_query = ("""CREATE (a:Node { q_id: $n_id , 
                                        label : $label , 
                                        label_short : $label_short , 
                                        loc_id : $loc_id , 
                                        mag_id : $mag_id , 
                                        part_of_flag : $part_of_flag , 
                                        instance_flag : $instance_flag , 
                                        subclass_flag : $subclass_flag , 
                                        in_osdg : $in_osdg }) """
    counter = 0
    for index, n_id in enumerate( ids ):
        result = [i for i in tx.run(check_query , n_id = n_id )]
        if result == [] :
            result = tx.run(insert_query, n_id = n_id , 
                                            label = data_to_insert[index]["label"] , 
                                            label_short = data_to_insert[index]["label_short"] , 
                                            loc_id = data_to_insert[index]["loc_id"] , 
                                            mag_id = data_to_insert[index]["mag_id"] , 
                                            part_of_flag = label = data_to_insert[index]["part_of_flag"] , 
                                            instance_flag = label = data_to_insert[index]["instance_flag"] ,
                                            subclass_flag = label = data_to_insert[index]["subclass_flag"] ,
                                            )
            counter += 1
        else:
            print(f"Node {n_id} already in graph; skipping it.")

    return counter


                            



    | Label | Def |
| ------ | ------ |
| q_id | Wiki Data ID |
| label | Wiki Data Label|
| label_short | Short version of Label |
| loc_id | Item's "Library of Congress" ID |
| mag_id | Item's ID in [MAG](https://academic.microsoft.com) |
| part_of_flag | Flag for whether parent class check was performed ( -1 - Not performed ; 0 - Performed, Negative ; 1 - Performed, Positive) |
| instance_flag | Flag for whether parent class check was performed ( -1 - Not performed ; 0 - Performed, Negative ; 1 - Performed, Positive)|
| subclass_flag | Flag for whether parent class check was performed ( -1 - Not performed ; 0 - Performed, Negative ; 1 - Performed, Positive)|
| in_osdg | Flag for whether the node is in OSDG |