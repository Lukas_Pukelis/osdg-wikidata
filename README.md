# Nodes

**TYPE** : Node


| Label | Def |
| ------ | ------ |
| q_id | Wiki Data ID |
| label | Wiki Data Label|
| label_short | Short version of Label |
| loc_id | Item's "Library of Congress" ID |
| mag_id | Item's ID in [MAG](https://academic.microsoft.com) |
| part_of_flag | Flag for whether parent class check was performed* |
| instance_flag | Flag for whether parent class check was performed* |
| subclass_flag | Flag for whether parent class check was performed* |
| in_osdg | Flag for whether the node is in OSDG** |

`* :  -1 - Not performed ; 0 - Performed, Result Negative ; 1 - Performed, Result Positive

`** : 1 - True ; 0 - False

# Edges

**Type** : part_of ( `P361` )

Relationship denoting that *instance* `a` is a member of **group** `b` 
| Label | Def |
| ------ | ------ |
| wiki_data_property | Code of the corresponding Wiki Data Property |

**Type** : instance_of ( `P31` )
Relationship denoting that `a` is an instance of **class** `b`
| Label | Def |
| ------ | ------ |
| wiki_data_property | Code of the corresponding Wiki Data Property |


**Type** : subclass_of ( `P279` )
Relationship denoting that `a` is a **child class** of `b`
| Label | Def |
| ------ | ------ |
| wiki_data_property | Code of the corresponding Wiki Data Property |


**Type** : osdg_mapped_to 
Relationship denoting that `a` linked to `b`via OSDG 
| Label | Def |
| ------ | ------ |
| - | - |



